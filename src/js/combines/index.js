$(document).ready(function(){
    let rightSection = $('#mainRightSection');
    let leftSection = $('#mainAddressSection');
    let toogleMapButton1 = $('#toggle-map-button');
    let toogleMapButton2 = $('#toggle-map-button2');
    let addressTitle = $('#addressTitle');
    let lineBar = $('#lineBar');

    toogleMapButton1.click(() => {
        rightSection.fadeOut();
        rightSection.addClass('d-none');
        leftSection.removeClass('col-xl-4');
        leftSection.addClass('col-xl-12');
        toogleMapButton1.addClass('d-xl-none');
        toogleMapButton2.addClass('d-xl-block');
        toogleMapButton1.removeClass('d-xl-block');
        toogleMapButton2.removeClass('d-xl-none');
        addressTitle.animate({left:'30%'},1000);
        lineBar.addClass('open');   
    });
    toogleMapButton2.click(() => {
        leftSection.removeClass('col-xl-12');
        leftSection.toggleClass('col-xl-4',2000).promise().done(()=>{
            console.log('fadsf')
        });
        setTimeout(() => {
            rightSection.removeClass('d-none');
            rightSection.fadeIn(300);
        }, 1000);
        toogleMapButton2.addClass('d-xl-none');
        toogleMapButton2.removeClass('d-xl-block');
        toogleMapButton1.addClass('d-xl-block');
        toogleMapButton1.removeClass('d-xl-none');
        addressTitle.animate({left:'27%'},1000);
        lineBar.removeClass('open'); 
    });
});


// $("#loader").toggleClass('fadeOut',600).promise().done(function(){
//     console.log('a');
// });